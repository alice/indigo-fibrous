import styled from 'indigo/fiber-declarative-elements/styled'
import element from 'indigo/fiber-declarative-elements/element'
import Heading from 'indigo/fibrous/components/Heading'


export default function Card({
    title,
    content,
    roundness,
    elevation,
    minWidth,
    padding,
    clicked,
    backgroundColor,
    textColor,
}) {

    return styled([`
        :host {
            display: block;
            min-width: ${minWidth || '100%'};
            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
            border-radius: ${roundness || 0};
            padding: ${padding || '10px'};
            background: ${backgroundColor || 'white'};
            color: ${textColor || 'white'};
        }

        :host:hover {
              box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
        }
    `])(element(() =>[
        title,
        content,
    ])())
}
