import styled from 'indigo/fiber-declarative-elements/styled'


export default function Heading(props) {

    return styled([`
        :host {
            font-size: 2rem;
            font-family: system-ui;
            font-weight: ${props.light ? 'normal' : 'bold'};
            color: ${props.color || 'black'};
        }
    `])(props.text)
}
