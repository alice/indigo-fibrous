import Heading from 'indigo/fibrous/components/Heading'
import Card from 'indigo/fibrous/components/Card'


export default function Button({ clicked, text, bold, ...rest }) {

    const card = Card({
        title: Heading({ text, light: !bold }),
        content: '',
        ...rest
    })

    card.onclick = clicked
    return card
}
